# HUGO DEPLOY

Ce projet modèle est pour déployer son site web [hugo](https://gohugo.io/) via le CI/CD du garage
Pour plus d'informations: [https://docs.legaragenumerique.xyz/divers/devops/cicd/](https://docs.legaragenumerique.xyz/divers/devops/cicd/)

## CONFIGURATION

1. Placer son site web dans le dossier hugo/ :
```bash
├── Dockerfile
├── hugo
│   ├── content
│   │   ├── page1.html
│   │   ├── page2.html
│   │   └── page3.html
│   └── config.toml
└── README.md
```

## PUBLICATION

1. Créer la branche deploy :
```bash
git checkout -b deploy
```

2. Pousser la branche deploy :
```bash
git push origin -u deploy
```

3. Le site est disponible à l'adresse https://username-nom_du_projet.legaragenumerique.xyz

exemple pour l'utilisateur greg avec un projet portfolio:
> https://greg-portfolio.legaragenumerique.xyz

:warning: Ne pas modifier la Dockerfile au risque que le projet ne soit pas déployé

## MISE A JOUR

- Pour mettre à jour son projet, pousser les updates sur la branche deploy

## DEPUBLICATION

1. Basculer sur une autre branche que deploy : 
```bash
git checkout main
```

2.  Effacer la branche en local :
```bash
git branch -d deploy
```

3. Effacer la branche distante : 
```bash
git push --delete origin deploy
```

> Le site n'est plus en ligne